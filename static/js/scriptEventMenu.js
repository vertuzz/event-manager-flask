var event_id = location['href'].split('/')[location['href'].split('/').length - 1];
var event_name;
var event_data = JSON.parse(localStorage.getItem(event_id));
animation_loader('show');
if(event_data){
    part_table_filling();
    costs_table_filling();
}// Начальная проверка свежести и присутсвия локальной копии данных
$(document).ready(function(){
    $.post("/listener/get_event_data",
        {
            event_id: event_id
        },
        function(data,status){
            if (status == 'success') {
                var response = data;
                animation_loader('hide');
                if (!event_data){
                    localStorage.setItem(event_id, JSON.stringify(response));
                    part_table_filling();
                    costs_table_filling();
                }else if(response.modified > event_data.modified){
                    localStorage.setItem(event_id, JSON.stringify(response));
                    table_clean();
                    costs_table_clean();
                    part_table_filling();
                    costs_table_filling();
                }
            }
        });
});

function main_menu(item) {
    var choice = item.value;
    var menu = document.getElementById('main_menu');
    var items = menu.getElementsByTagName('LI');
    var part_menu = document.getElementById('part_menu_wrapper');
    var costs_menu = document.getElementById('costs_menu_wrapper');
    var budget_menu = document.getElementById('budget_menu_wrapper');
    if(choice == 1){
        items[0].className = 'active';
        items[1].className = '';
        items[2].className = '';
        part_menu.style.display = 'block';
        costs_menu.style.display = 'none';
        budget_menu.style.display = 'none';
    }else if(choice == 2) {
        items[0].className = '';
        items[1].className = 'active';
        items[2].className = '';
        part_menu.style.display = 'none';
        costs_menu.style.display = 'block';
        budget_menu.style.display = 'none';
    }else if(choice == 3){
        items[0].className = '';
        items[1].className = '';
        items[2].className = 'active';
        part_menu.style.display = 'none';
        costs_menu.style.display = 'none';
        budget_menu.style.display = 'block';
    }
}

function part_table_filling() {
    event_data = JSON.parse(localStorage.getItem(event_id));
    var table = document.getElementById('part-list');
    var tbody = document.createElement('tbody');
    tbody.setAttribute('id', 'part_list_body');
    table.appendChild(tbody);
    event_name = event_data.event_name;
    document.getElementById('event_name').innerHTML = 'Мероприятие: ' + event_name;
    for (var i = 0; i < event_data.participants.length; i++) {
        insert_part_row(i+1, event_data.participants[i].name, event_data.participants[i].income,
            event_data.participants[i].debt)
    }

    var row = document.createElement("TR");

    var td1 = document.createElement("TD");
    var td2 = document.createElement("TD");
    var td3 = document.createElement("TD");
    var td4 = document.createElement("TD");
    var td5 = document.createElement("TD");
    
    td2.appendChild(document.createTextNode('Итог:'));
    td3.appendChild(document.createTextNode(event_data.income));
    td3.setAttribute('id', 'total-income');
    td4.appendChild(document.createTextNode(event_data.debt));
    td4.setAttribute('id', 'total-debt');
    var butt = document.createElement('button');
    butt.className = 'btn btn-primary';
    butt.addEventListener('click', save);
    butt.appendChild(document.createTextNode('Подтвердить'));
    td5.appendChild(butt);

    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);

    tbody.appendChild(row);

    document.getElementById('amount_of_participants').innerHTML = event_data.participants.length;
    document.getElementById('infoBudget').innerHTML = event_data.budget;
    document.getElementById('infoDebt').innerHTML = event_data.debt;
    document.getElementById('infoIncome').innerHTML = event_data.income;

    change_row_color();
}

function costs_table_filling() {
    event_data = JSON.parse(localStorage.getItem(event_id));
    var table = document.getElementById('costs_list');
    var tbody = document.createElement('tbody');
    tbody.setAttribute('id', 'costs_list_body');
    table.appendChild(tbody);
    for (var i = 0; i < event_data.costs.length; i++) {
        insert_cost_row(i+1, event_data.costs[i].cost_name, event_data.costs[i].cost_value)
    }
    var total_body = document.createElement("tbody");
    total_body.setAttribute('id', 'total_costs_body');
    var row = document.createElement("TR");
    row.setAttribute('id', 'costs_total_info');

    var td1 = document.createElement("TD");
    var td2 = document.createElement("TD");
    var td3 = document.createElement("TD");

    td2.appendChild(document.createTextNode('Сумма:'));
    td3.appendChild(document.createTextNode(costs_sum()));
    td3.setAttribute('id', 'costs_sum');

    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);

    total_body.appendChild(row);
    table.appendChild(total_body);
    budget_table_conroller();
}

function insert_part_row(index, name, income, debt) {
    tbody = document.getElementById('part_list_body');
    var total_info = document.getElementById('total_info');
    var row = document.createElement("TR");

    var td1 = document.createElement("TD");
    var td2 = document.createElement("TD");
    var td3 = document.createElement("TD");
    var td4 = document.createElement("TD");
    var td5 = document.createElement("TD");
    var span_for_del = document.createElement("span");
    span_for_del.className = "glyphicon glyphicon-remove";
    span_for_del.setAttribute('name', 'part_del_glyphs');
    span_for_del.style.color = '#b92c28';
    span_for_del.style.visibility = 'hidden';
    span_for_del.addEventListener('click', function(){del_part(index);});

    td1.appendChild(document.createTextNode(index));
    td2.appendChild(document.createTextNode(name));
    td1.appendChild(span_for_del);
    td2.setAttribute('name', 'person_name');
    td3.appendChild(document.createTextNode(income));
    td3.setAttribute('name', 'person_income');
    td4.appendChild(document.createTextNode(debt));
    td4.setAttribute('name', 'person_debt');
    var input = document.createElement('input');
    input.className = 'form-control';
    input.setAttribute('type', 'number');
    input.addEventListener('keydown', function () {
        if(event.keyCode==13)
       {
           save();
       }
    });
    td5.appendChild (input);

    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);
    
    tbody.appendChild(row);

}

function insert_cost_row(index, name, sum) {
    tbody = document.getElementById('costs_list_body');
    var row = document.createElement("TR");

    var td1 = document.createElement("TD");
    var td2 = document.createElement("TD");
    var td3 = document.createElement("TD");
    var span_for_del = document.createElement("span");
    span_for_del.className = "glyphicon glyphicon-remove";
    span_for_del.setAttribute('name', 'cost_del_glyphs')
    span_for_del.style.color = '#b92c28';
    span_for_del.style.visibility = 'hidden';
    span_for_del.addEventListener('click', function(){del_cost(index);});

    td1.appendChild(document.createTextNode(index));
    td1.appendChild(span_for_del);
    td2.appendChild(document.createTextNode(name));
    td2.setAttribute('name', 'costs_name');
    var input = document.createElement('input');
    input.className = 'form-control';
    input.setAttribute('type', 'number');
    input.setAttribute('name', 'costs_val');
    input.setAttribute('value', sum);
    input.addEventListener('keydown', function () {
        if(event.keyCode==13)
       {
           save_costs();
       }
    });
    td3.appendChild(input);

    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);

    tbody.appendChild(row);
}

function table_clean() {
    var table_body = document.getElementById('part_list_body');
    table_body.remove();
}

function costs_table_clean() {
    document.getElementById('costs_list_body').remove();
    document.getElementById('total_costs_body').remove();
}


function save() {
    event_data = JSON.parse(localStorage.getItem(event_id));
    var tableElem = document.getElementById('part-list'); // Таблица
    var inputs = tableElem.getElementsByTagName('input'); // Все формы в Таблице
    var personDebts = document.getElementsByName('person_debt'); // Задолженности участников
    var personIncomes = document.getElementsByName('person_income'); // Вклады участников
    var personNames = document.getElementsByName('person_name'); // Имена участников
    var totalDebt = document.getElementById('total-debt'); // Общий долг
    var totalIncome = document.getElementById('total-income'); // Общий взнос
    var budget = document.getElementById('infoBudget');
    var newDebt = 0;
    var newInc = 0;
    var new_debts = [];
    var new_incomes = [];
    var part_datas = [];

    for (var i = 0; i < personDebts.length; i++) { // Пересчет задолженностей
        personDebts[i].innerHTML = ((+personDebts[i].innerHTML) - (+inputs[i].value)).toFixed(2);
        personIncomes[i].innerHTML = ((+personIncomes[i].innerHTML) + (+inputs[i].value)).toFixed(2);
        inputs[i].value = '';
        new_debts.push(personDebts[i].innerHTML);
        new_incomes.push(personIncomes[i].innerHTML);
        newDebt += (+personDebts[i].innerHTML);
        newInc += (+personIncomes[i].innerHTML);
        part_datas.push({debt: personDebts[i].innerHTML, income: personIncomes[i].innerHTML,
                        name: event_data.participants[i].name})
    }
    totalIncome.innerHTML = document.getElementById('infoIncome').innerHTML = newInc.toFixed(); // Новый общий взнос
    totalDebt.innerHTML = document.getElementById('infoDebt').innerHTML = newDebt.toFixed();  // Новая общая задоженность

    change_row_color();
    update_event_data(budget.innerHTML, newDebt, newInc, part_datas, event_data.costs);
}

function showSumForm(){
    var tip =  document.querySelector('[name="optradio"]:checked').value;
    if(tip == 1){
        document.getElementById('formForSum').style.display = "none";
    }else {
        document.getElementById('new_sum').value = (event_data.budget / event_data.participants.length).toFixed(2);
        document.getElementById('formForSum').style.display = "block";
    }

}

function change_budget(budget) {
    event_data = JSON.parse(localStorage.getItem(event_id));
    var part_data = event_data.participants;
    var new_part_data = [];
    var new_part_debt = parseFloat(budget) / part_data.length;
    var diff = parseFloat(budget) - event_data.budget;
    for (var i = 0; i < part_data.length; i++) {
        new_part_data.push({name: part_data[i].name, income: part_data[i].income,
                            debt: (new_part_debt - part_data[i].income).toFixed(2)});
    }
    update_event_data(budget, parseFloat(budget) - event_data.income, event_data.income, new_part_data, event_data.costs);
    table_clean();
    part_table_filling();
}

function equals_budget_to_costs(){
    change_budget(costs_sum())
}

function add_part() {
    event_data = JSON.parse(localStorage.getItem(event_id));
    var part_data = event_data.participants;
    var new_part_name = document.getElementById('new_name').value;
    var option = document.querySelector('[name="optradio"]:checked').value;
    var new_part_data = [];
    var new_part_debt;
    var updated_event_data = {};
    if (option == 1){
        new_part_debt = (event_data.budget / (part_data.length +1)).toFixed(2);
        for (var i = 0; i < part_data.length; i++) {
            new_part_data.push({name: part_data[i].name, income: part_data[i].income,
                                debt: (new_part_debt - part_data[i].income).toFixed(2)});
        }
        new_part_data.push({name: new_part_name, income:0, debt: new_part_debt});
        update_event_data(event_data.budget, event_data.debt, event_data.income, new_part_data, event_data.costs)
    }else {
        new_part_debt = parseFloat(document.getElementById('new_sum').value);
        new_part_data = part_data;
        new_part_data.push({name: new_part_name, income:0, debt: new_part_debt});
        update_event_data(parseFloat(event_data.budget) + new_part_debt, parseFloat(event_data.debt) + new_part_debt,
        event_data.income, new_part_data, event_data.costs);
    }
    table_clean();
    part_table_filling();
    
}
//Delete participant
function del_part(index) {
    $('#delMenu').modal('show');
    event_data = JSON.parse(localStorage.getItem(event_id));
    var part_data = event_data.participants;
    var del_part_income = parseFloat(part_data[index-1].income);
    var new_part_data = [];
    var new_part_debt;
    $('#name_for_del').text(part_data[index-1].name);
    $('#income_for_del').text(part_data[index-1].income);

    $('#del_confirm').click(function(){
        var option = $('input[name=del_opt]:checked').val();
        part_data.splice(index-1, 1);
        if (option == 1){
            new_part_debt = (event_data.budget / (part_data.length)).toFixed(2);
            for (var i = 0; i < part_data.length; i++) {
                new_part_data.push({name: part_data[i].name, income: part_data[i].income,
                                    debt: (new_part_debt - part_data[i].income).toFixed(2)});
            }
            update_event_data(event_data.budget, parseFloat(event_data.debt) + del_part_income, event_data.income - del_part_income,
            new_part_data, event_data.costs)
        }else {
            var reducing = (event_data.budget / (part_data.length+1));
            update_event_data(parseFloat(event_data.budget) - reducing, parseFloat(event_data.debt) - reducing + del_part_income,
            event_data.income - del_part_income, part_data, event_data.costs)
            }
        location.reload();
    })
}
// Show del participant icons
$(document).ready(function(){
    $('#del_part').click(function(){
        var glyphs = document.getElementsByName('part_del_glyphs');
        if (glyphs[0].style.visibility == 'visible'){
            for (var i = 0; i < glyphs.length; i++){
            glyphs[i].style.visibility = 'hidden';
            }
        }else {
            for (var f = 0; f < glyphs.length; f++) {
                glyphs[f].style.visibility = 'visible';
            }
        }
    });
    $('#del_cost').click(function(){
        var glyphs = document.getElementsByName('cost_del_glyphs');
        if (glyphs[0].style.visibility == 'visible'){
            for (var i = 0; i < glyphs.length; i++){
            glyphs[i].style.visibility = 'hidden';
            }
        }else {
            for (var f = 0; f < glyphs.length; f++) {
                glyphs[f].style.visibility = 'visible';
            }
        }
    });
});

function change_row_color() {
    var personDebts = document.getElementsByName('person_debt'); // Задолженности участников
    var personIncomes = document.getElementsByName('person_income'); // Вклады участников
    var wholeDebt = document.getElementById('total-debt'); // Общий долг
    var totalIncome = document.getElementById('total-income'); // Общий взнос
    var Budget = document.getElementById('infoBudget').innerHTML;

    var bad_debt = +Budget / personDebts.length;

    for (var i = 0; i < personDebts.length; i++){
        if (parseFloat(personDebts[i].innerHTML) >= bad_debt*0.7){
            $('tr:eq(' + (i+1) + ')').removeClass().addClass("danger");
        }else if(parseFloat(personDebts[i].innerHTML) >= bad_debt*0.1){
            $('tr:eq(' + (i+1) + ')').removeClass().addClass("warning");
        }else if(parseFloat(personDebts[i].innerHTML) >= 0){
            $('tr:eq(' + (i+1) + ')').removeClass().addClass("success");
        }else {
            $('tr:eq(' + (i+1) + ')').removeClass().addClass("success");
        }
    }

    if (parseFloat(wholeDebt.innerHTML) >= +Budget*0.7){
        $('#total-debt').removeClass().addClass("danger");
        $('#total-income').removeClass().addClass("danger");
    }else if(parseFloat(wholeDebt.innerHTML) >= +Budget*0.1){
        $('#total-debt').removeClass().addClass("warning");
        $('#total-income').removeClass().addClass("warning");
    }else if(parseFloat(wholeDebt.innerHTML) >= 0){
        $('#total-debt').removeClass().addClass("success");
        $('#total-income').removeClass().addClass("success");
    }else {
        $('#total-debt').removeClass().addClass("success");
        $('#total-income').removeClass().addClass("success");
    }


}

function animation_loader(opt) {
    if (opt == 'show'){
        document.getElementById('ballsWaveG').style.visibility = "visible";
    }else {
        document.getElementById('ballsWaveG').style.visibility = "hidden";
    }

}
function send_event_data(updated_event_data) {
    animation_loader('show');
    localStorage.setItem(event_id, updated_event_data);
    budget_table_conroller();
    $.post("/listener/update_event_data",
        {
            event_data: updated_event_data
        },
        function(data,status){
            if (status == 'success') {
                var response = data;
                animation_loader('hide');
            }
        });
}

function costs_sum() {
    var costs = document.getElementsByName('costs_val');
    var sum = 0;
    for (var i = 0; i < costs.length; i++){
        sum += parseFloat(costs[i].value);
    }
    document.getElementById('infoCosts_sum').innerHTML = sum.toString();
    return sum
}

function save_costs() {
    event_data = JSON.parse(localStorage.getItem(event_id));
    var cost_names = document.getElementsByName('costs_name');
    var cost_values = document.getElementsByName('costs_val');
    var costs_list = [];

    for (var i = 0; i < cost_names.length; i++){
        costs_list.push({cost_name: cost_names[i].innerHTML, cost_value: parseFloat(cost_values[i].value).toFixed(2)});
    }
    document.getElementById('costs_sum').innerHTML = costs_sum();
    update_event_data(event_data.budget, event_data.budget, event_data.income, event_data.participants, costs_list);
}

function update_event_data(budget, debt, income, participants, costs) {

    var updated_event_data = JSON.stringify({budget: parseFloat(budget).toFixed(), debt: parseFloat(debt).toFixed(),
                                            income: parseFloat(income).toFixed(), event_name: event_name, costs: costs,
                                            participants: participants, event_id: event_id, modified: Date.now()});

    send_event_data(updated_event_data);
}

function add_cost() {
    var index = document.getElementsByName('costs_name').length +1;
    var cost_name = document.getElementById('new_cost_name').value;
    var cost_val = document.getElementById('new_cost_sum').value;
    insert_cost_row(index, cost_name, cost_val);
    save_costs();
}

function del_cost(index) {
    event_data = JSON.parse(localStorage.getItem(event_id));
    var costs_list = event_data.costs;
    costs_list.splice(index-1, 1);
    update_event_data(event_data.budget, event_data.budget, event_data.income, event_data.participants, costs_list);
    document.getElementById('costs_list_body').remove();
    document.getElementById('total_costs_body').remove();
    costs_table_filling();
}

function budget_table_conroller() {
    event_data = JSON.parse(localStorage.getItem(event_id));

    var bt_budget = document.getElementById('bt_budget');
    var bt_part_pay = document.getElementById('bt_part_pay');
    var bt_costs_sum = document.getElementById('bt_costs_sum');
    var bt_diff = document.getElementById('bt_diff');
    var costs = costs_sum();

    bt_budget.value = event_data.budget;
    bt_part_pay.value = (event_data.budget / event_data.participants.length).toFixed(2);
    bt_costs_sum.innerHTML = costs;
    bt_dif_controller(costs - event_data.budget);
}

function bt_change_budget(elem) {
    var amount_of_participants = document.getElementById('amount_of_participants').innerHTML
    var bt_budget = document.getElementById('bt_budget');
    var bt_part_pay = document.getElementById('bt_part_pay');
    var bt_costs_sum = document.getElementById('bt_costs_sum');
    var bt_diff = document.getElementById('bt_diff');
    if (elem.getAttribute('id') == 'bt_budget'){
        bt_part_pay.value = (bt_budget.value / amount_of_participants).toFixed(2);
        bt_dif_controller(bt_costs_sum.innerHTML - bt_budget.value);
    }else if(elem.getAttribute('id') == 'bt_part_pay'){
        bt_budget.value = (bt_part_pay.value * amount_of_participants).toFixed(2);
        bt_dif_controller(bt_costs_sum.innerHTML - bt_budget.value);
    }
}

function bt_dif_controller(diff) {
    var bt_diff_name = document.getElementById('bt_diff_name');
    var bt_diff = document.getElementById('bt_diff');

    if (diff < 0){
        bt_diff_name.innerHTML = 'Бюджет привышает расходы на:';
        bt_diff_name.parentNode.className = 'success';
    }else if(diff == 0){
        bt_diff_name.innerHTML = 'Бюджет равен расходам';
        bt_diff_name.parentNode.className = 'success';
    }else {
        bt_diff_name.innerHTML = 'Расходы превышают бюджет на:';
        bt_diff_name.parentNode.className = 'danger';
    }

    bt_diff.innerHTML = Math.abs(diff);
}