function delete_event(event_id_for_del){
        if (confirm('Вы уверены, что хотите удалить мероприятие?')) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    localStorage.removeItem(event_id_for_del);
                    location.reload();
                }
            };
            xmlhttp.open("POST", "/listener/del_event", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send("event_id_for_del=" + event_id_for_del);
        }

    }
