# coding=utf-8
from flask import Flask, render_template, request, redirect, url_for, session, json
from flask_pymongo import PyMongo, ObjectId
from assets import assets_config
import config

app = Flask(__name__)
assets_config(app)

app.config['MONGO_DBNAME'] = config.mongo_db_name
app.config['MONGO_URI'] = config.mongo_uri
app.secret_key = config.secret_key

mongo = PyMongo(app)


@app.route('/')
def index():
    if 'username' in session:
        events_list = mongo.db[session['username'] + '_events'].find()
        return render_template('index.html', events_list=events_list)
    return render_template('index.html')


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/new_event', methods=['GET', 'POST'])
def new_event():
    if 'username' in session:
        return render_template('new_event.html')
    else:
        return redirect('/')


@app.route('/event_menu/')
@app.route('/event_menu/<ObjectId:event_id>')
def event_menu(event_id=None):

    return render_template('event_menu.html')


@app.route('/listener/', methods=['GET', 'POST'])
@app.route('/listener/<option>', methods=['GET', 'POST'])
def listener(option):
    if request.method == 'POST':
        if option == 'create_event':
            events = mongo.db[session['username'] + '_events']
            new_event_data = json.loads(request.form['dataForSave'])
            event_id = events.insert_one(new_event_data).inserted_id
            return str(event_id)
        elif option == 'get_event_data':
            events = mongo.db[session['username'] + '_events']
            event_id = request.form['event_id']
            event_data = dict(events.find_one({"_id": ObjectId(event_id)}))
            event_data.pop('_id')
            return json.jsonify(event_data)

        elif option == 'del_event':
            event_id_for_del = request.form['event_id_for_del']
            res = mongo.db[session['username'] + '_events'].find_one_and_delete({"_id": ObjectId(event_id_for_del)})
            return "Мероприятие удалено"
        elif option == 'update_event_data':
            new_event_data = json.loads(request.form['event_data'])
            event_id = new_event_data['event_id']
            data_base = mongo.db[session['username'] + '_events']

            data_base.update_one({"_id": ObjectId(event_id)},
                                 {'$set': {'debt': new_event_data['debt'],
                                           'income': new_event_data['income'],
                                           'participants': new_event_data['participants'],
                                           'costs': new_event_data['costs'],
                                           'modified': new_event_data['modified']}})
            return 'success'
    return "Error"


@app.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'POST':
        error_list = []
        prev_values = {}
        if len(request.form['username']) < 4:
            error_list.append('Имя слишком короткое (не менее 4 символов)')
        if len(request.form['pass']) < 6:
            error_list.append('Пароль слишком короткий (не менее 6 символов)')
        if error_list:
            prev_values['username'] = request.form['username']
            return render_template('register.html', error_list=error_list,
                                   prev_values=prev_values)
        users = mongo.db.users
        new_user = str(request.form['username']).strip().lower()
        existing_user = users.find_one({'name': new_user})

        if existing_user is None:
            users.insert({'name': new_user, 'password': request.form['pass']})
            session['username'] = new_user
            return redirect(url_for('index'))

        return render_template('register.html', error_list=['Пользователь с таким логином уже существует'])
    return render_template('register.html')


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        users = mongo.db.users
        user_input = str(request.form['username']).strip().lower()
        login_user = users.find_one({'name': user_input})

        if login_user:
            if request.form['pass'] == login_user['password']:
                session['username'] = user_input
                return redirect(url_for('index'))

        return render_template('login.html', error='Неправильная комбинция логина и пароля')
    return render_template('login.html')


@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.debug = True
    app.run()
