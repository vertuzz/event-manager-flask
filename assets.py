from flask_assets import Environment, Bundle


def assets_config(app):
    assets = Environment(app)
    js_event_menu = Bundle('js/scriptEventMenu.js', output='gen/js_event_menu.js')
    js_new_event = Bundle('js/scriptNewEvent.js', output='gen/js_new_event.js')
    js_index = Bundle('js/scriptIndex.js', output='gen/js_index.js')
    js_reg = Bundle('js/scriptReg.js', output='gen/js_reg.js')

    assets.register('js_event_menu', js_event_menu)
    assets.register('js_new_event', js_new_event)
    assets.register('js_index', js_index)
    assets.register('js_reg', js_reg)